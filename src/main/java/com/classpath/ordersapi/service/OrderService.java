package com.classpath.ordersapi.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.classpath.ordersapi.exception.InvalidOrderException;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class OrderService {
	
	@Autowired
	private OrderRepository orderRepository;
	
	public Order save(Order order) {
		return this.orderRepository.save(order);
	}
	
	public Set<Order> fetchOrders(){
		return new HashSet<>(this.orderRepository.findAll());
	}
	
	public Order fetchOrderById(long orderId) {
		
		/*
		 * Optional<Order> optionalOrder = this.orderRepository.findById(orderId);
		 * 
		 * if (optionalOrder.isEmpty()) { throw new
		 * IllegalArgumentException("Invalid Order Id"); } return optionalOrder.get();
		 */
		
		return this.orderRepository.findById(orderId)
				.orElseThrow(() -> new InvalidOrderException("Invalid order id"));
	}
	
	public void deleteOrderById(long orderId) {
		this.orderRepository.deleteById(orderId);
	}
	
	public Set<Order> fetchOrdersByCustomerName(String customerName){
		return new HashSet<Order>(this.orderRepository.fetchOrdersByCustomerName(customerName));
	}
}
