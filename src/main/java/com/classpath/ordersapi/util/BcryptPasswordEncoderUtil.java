package com.classpath.ordersapi.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BcryptPasswordEncoderUtil {
	
	public static void main(String args[]) {
	
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encoded1 = passwordEncoder.encode("welcome");
		String encoded2 = passwordEncoder.encode("welcome");
		String encoded3 = passwordEncoder.encode("welcome");
		String encoded4 = passwordEncoder.encode("welcome");
		
		System.out.println(encoded1);

		System.out.println(encoded2);
		System.out.println(encoded3);
		System.out.println(encoded4);
		
		System.out.println(passwordEncoder.matches("welcome", encoded4));
		System.out.println(passwordEncoder.matches("welcome.", encoded3));
		System.out.println(passwordEncoder.matches("welcome", encoded2));
		System.out.println(passwordEncoder.matches("welcome", encoded1));
		
	}

}
