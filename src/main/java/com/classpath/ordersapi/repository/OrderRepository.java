package com.classpath.ordersapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.classpath.ordersapi.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
	
	public List<Order> findByCustomerNameAndOrderPrice(String customerName, double orderPrice);
	
	 @Query("select order from Order order where order.customerName = ?1")
	 public List<Order> fetchOrdersByCustomerName(String customerName);

}
