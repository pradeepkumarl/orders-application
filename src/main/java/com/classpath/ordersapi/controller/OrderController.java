package com.classpath.ordersapi.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.OrderService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@PostMapping
	public Order createOrder(@RequestBody Order order ) {
		System.out.println("Order ========");
		System.out.println(order);
		return this.orderService.save(order);
	}
	
	@GetMapping
	public Set<Order> fetchAllOrders(){
		return this.orderService.fetchOrders();
	}
	
	@GetMapping("/{id}")
	public Order fetchOrderById(@PathVariable("id")long orderId) {
		return this.orderService.fetchOrderById(orderId);
	}
	
	@GetMapping("/customers/{customer}")
	public Set<Order> fetchOrderById(@PathVariable("customer")String customerName) {
		return this.orderService.fetchOrdersByCustomerName(customerName);
	}
	
	@DeleteMapping("/{id}")
	public void deleteOrderById(@PathVariable("id")long orderId) {
		 this.orderService.deleteOrderById(orderId);
	}
}
