package com.classpath.ordersapi.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {
	
	
	@Bean
	@ConditionalOnProperty(name = "app.loadUser", havingValue = "false", matchIfMissing = false)
	public User user() {
		return new User();
	}
	

	@Bean(name = "conditionOnBeanUser")
	@ConditionalOnMissingBean(name = "user")
	public User conditionOnBeanUser() {
		return new User();
	}

}

class User {
	
}
