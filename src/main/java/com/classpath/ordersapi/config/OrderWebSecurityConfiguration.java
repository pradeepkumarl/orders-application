package com.classpath.ordersapi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.classpath.ordersapi.service.DomainUserDetailsService;

@Configuration

public class OrderWebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DomainUserDetailsService userDetailsService;
	
	
	
	@Override
	public void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
		authManagerBuilder
			.userDetailsService(this.userDetailsService)
			.passwordEncoder(passwordEncoder());

	}
	
	 @Override
	 protected void configure(HttpSecurity httpSecurity) throws Exception {
		 httpSecurity.cors().disable();
	     httpSecurity.csrf().disable();
	     httpSecurity.headers().frameOptions().disable();
	     
	     httpSecurity
         .authorizeRequests()
         .antMatchers(HttpMethod.GET,"/api/v1/orders/**")
         .hasAnyRole("USER", "ADMIN")
         .and()
         .authorizeRequests()
         .antMatchers(HttpMethod.POST,"/api/v1/orders/**")
         .hasRole("ADMIN")
         .antMatchers(HttpMethod.PUT,"/api/v1/orders/**")
         .hasRole("ADMIN")
         .antMatchers(HttpMethod.DELETE,"/api/v1/orders/**")
         .hasRole("ADMIN")
         .anyRequest()
         .authenticated()
         .and()
         .formLogin()
         .and()
         .httpBasic();
	 }
	
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}


}
