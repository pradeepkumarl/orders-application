package com.classpath.ordersapi.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class DomainHealthCheckEndpoint implements HealthIndicator{

	@Override
	public Health health() {
		//check the DB and execute a simple select statement
		//connect to a REST endpoint and check if the service is up 
		return Health.up().withDetail("DB service", "DB is up").build();
	}

}

@Component
class DomainRestCheckEndpoint implements HealthIndicator{

	@Override
	public Health health() {
		//check the DB and execute a simple select statement
		//connect to a REST endpoint and check if the service is up 
		return Health.up().withDetail("Payment service", "Payment Gateway is up").build();
	}

}
