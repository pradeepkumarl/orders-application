package com.classpath.ordersapi;

import java.util.List;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class OrdersApiApplication {
	
	@Autowired
	private ApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplication.run(OrdersApiApplication.class, args);
    }

	
	public void run(String... args) throws Exception {
		List<String> beans = Arrays.asList(applicationContext.getBeanDefinitionNames());
		
		//beans.forEach(bean -> System.out.println(bean));
	}

}
