package com.classpath.ordersapi.exception;

public class InvalidOrderException extends RuntimeException {
	
	public InvalidOrderException(String message) {
		super(message);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}

}
